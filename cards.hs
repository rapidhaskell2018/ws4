#!/usr/bin/env stack
-- stack runhaskell --resolver lts-12.13 --package rio

{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

import RIO

data Value = Two | Three | Four | Five | Six |
             Seven | Eight | Nine | Ten |
             Jack | Queen | King | Ace
   deriving (Eq, Ord, Enum, Show, Bounded)

data Suit = Hearts | Clubs | Spades | Diamonds
   deriving (Eq, Ord, Enum, Show, Bounded)

type Card = (Value, Suit)
type Desk = [Card]

deck = liftA2 (,) [Two ..] [Hearts ..]

main = runSimpleApp $ do
  logInfo $ displayShow $ deck
  logInfo $ displayShow $ length $ deck
