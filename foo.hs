#!/usr/bin/env stack
-- stack runhaskell --resolver lts-12.13 --package rio

{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

import RIO
import RIO.Process

data V3 a = V3 a a a
  deriving (Show, Eq)

instance Functor V3 where
  fmap f (V3 x y z) = V3 (f x) (f y) (f z)

instance Foldable V3 where
  foldr f a (V3 x y z) = z `f` (y `f` (x `f` a))

main = runSimpleApp $ do
  logInfo $ "Hello World"
  proc "touch" ["tiny kitten"] (runProcess_)
  (c, s, e) <- proc "uname" [] (readProcess)
  logInfo $ displayShow $ s
