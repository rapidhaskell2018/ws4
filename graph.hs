#!/usr/bin/env stack
{- stack runhaskell --resolver lts-12.13
--package rio --package algebraic-graphs -}

{-# LANGUAGE NoImplicitPrelude #-}

import RIO
import RIO.List.Partial
import Algebra.Graph

myGraph :: Graph String
myGraph = overlay
  (vertex "spatula")
  (connects $ [vertex "quux",
               vertices ["foo", "bar", "baz"]

main = runSimpleApp $ do
  logInfo $ displayShow $ myGraph
  logInfo $ displayShow $ foldr (++) "" myGraph
  let x = sequenceA myGraph -- `[Graph Char]`
  logInfo $ displayShow $ x
  logInfo $ displayShow $ length x
  logInfo $ displayShow $ head x
  logInfo $ displayShow $ (x !! 1)
  logInfo $ displayShow $ (x !! 2)
